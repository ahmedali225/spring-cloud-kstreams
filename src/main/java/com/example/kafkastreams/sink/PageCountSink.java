package com.example.kafkastreams.sink;

import com.example.kafkastreams.config.AnalyticsBinding;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * Created by aali on 2020-02-25
 **/
@Component
@Slf4j
@EnableBinding(AnalyticsBinding.class)
public class PageCountSink {

    @StreamListener
    public void process(@Input(AnalyticsBinding.PAGE_COUNT_IN) KTable<String, Long> counts) {
        counts.toStream()
                .foreach((key, value) -> log.info(key + "==" + value));
    }
}
