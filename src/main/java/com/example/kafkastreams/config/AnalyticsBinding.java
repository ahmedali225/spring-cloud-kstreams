package com.example.kafkastreams.config;

import com.example.kafkastreams.processor.PageCount;
import com.example.kafkastreams.source.PageViewEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

/**
 * Created by aali on 2020-02-25
 **/
public interface AnalyticsBinding {

    String PAGE_VIEWS_IN = "pvin";

    String PAGE_VIEWS_OUT = "pvout";

    String PAGE_COUNT_MV = "pcmv";

    String PAGE_COUNT_OUT = "pcout";

    String PAGE_COUNT_IN = "pcin";

    @Input(PAGE_VIEWS_IN)
    KStream<String, PageViewEvent> pageViewsIn();

    @Output(PAGE_VIEWS_OUT)
    MessageChannel pageViewsOut();

    @Output(PAGE_COUNT_OUT)
    KStream<String, String> pageCountsOut();

    @Input(PAGE_COUNT_IN)
    KTable<String, Long> pageCountsIn();
}
