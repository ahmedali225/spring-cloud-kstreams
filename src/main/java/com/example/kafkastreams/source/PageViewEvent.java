package com.example.kafkastreams.source;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by aali on 2020-02-25
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageViewEvent {
    private String userId, page;
    private long duration;
}
