package com.example.kafkastreams.source;

import com.example.kafkastreams.config.AnalyticsBinding;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by aali on 2020-02-25
 **/
@Component
@AllArgsConstructor
@Slf4j
@EnableBinding(AnalyticsBinding.class)
public class PageViewEventSource implements ApplicationRunner {

    private final AnalyticsBinding sourceBinder;

    @Override
    public void run(ApplicationArguments args) {
        List<String> names = Arrays.asList("mfisher", "dyser", "schako", "abilan", "grussell", "ozko");
        List<String> pages = Arrays.asList("blog", "sitemap", "initializer", "news", "contact", "about");

        Runnable runnable = () -> {
            String rPage = pages.get(new Random().nextInt(pages.size()));
            String rName = names.get(new Random().nextInt(names.size()));

            PageViewEvent pageViewEvent = new PageViewEvent(rName, rPage, Math.random() > .5 ? 10 : 1000);

            final Message<PageViewEvent> message = MessageBuilder.withPayload(pageViewEvent)
                    .setHeader(KafkaHeaders.MESSAGE_KEY, pageViewEvent.getUserId().getBytes())
                    .build();
            try {
                this.sourceBinder.pageViewsOut().send(message);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        };

        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(runnable, 1, 1, TimeUnit.SECONDS);
    }
}
