package com.example.kafkastreams.processor;

import com.example.kafkastreams.config.AnalyticsBinding;
import com.example.kafkastreams.source.PageViewEvent;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * Created by aali on 2020-02-25
 **/
@Component
@EnableBinding(AnalyticsBinding.class)
public class PageViewEventProcessor {

    @StreamListener
    @SendTo(AnalyticsBinding.PAGE_COUNT_OUT)
    public KStream<String, Long> process(@Input(AnalyticsBinding.PAGE_VIEWS_IN) KStream<String, PageViewEvent> events) {
        return events.filter((key, value) -> value.getDuration() > 10)
                .map((key, value) -> new KeyValue<>(value.getPage(), "0"))
                .groupByKey()
//                .windowedBy(TimeWindows.of(Duration.ofMillis(1000)))
                .count(Materialized.as(AnalyticsBinding.PAGE_COUNT_MV))
                .toStream();
//                .map((key, value) -> new KeyValue<>(null, new PageCount(key.key(), value)));
    }
}
