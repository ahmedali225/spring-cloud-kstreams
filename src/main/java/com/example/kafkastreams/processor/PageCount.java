package com.example.kafkastreams.processor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by aali on 2020-02-26
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PageCount {

    private String page;

    private Long count;
}
