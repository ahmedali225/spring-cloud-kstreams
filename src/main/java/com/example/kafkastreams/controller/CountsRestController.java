package com.example.kafkastreams.controller;

import com.example.kafkastreams.config.AnalyticsBinding;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aali on 2020-02-26
 **/
@RestController
public class CountsRestController {

    private final InteractiveQueryService registry;

    public CountsRestController(InteractiveQueryService registry) {
        this.registry = registry;
    }

    @GetMapping("/counts")
    Map<String, Long> counts() {
        ReadOnlyKeyValueStore<String, Long> store = registry.getQueryableStore(AnalyticsBinding.PAGE_COUNT_MV, QueryableStoreTypes.keyValueStore());

        Map<String, Long> m = new HashMap<>();
        KeyValueIterator<String, Long> iterator = store.all();
        while (iterator.hasNext()) {
            KeyValue<String, Long> next = iterator.next();
            m.put(next.key, next.value);
        }
        return m;
    }
}
